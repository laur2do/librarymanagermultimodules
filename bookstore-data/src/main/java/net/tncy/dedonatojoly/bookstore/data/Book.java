package net.tncy.dedonatojoly.bookstore.data;

import net.tncy.validator.constraints.books.ISBN;

public class Book {

    private String title;
    private String author;
    @ISBN
    private String codeISBN;


    public Book(String title, String author, String codeISBN) {
        this.title = title;
        this.author = author;
        this.codeISBN = codeISBN;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCodeISBN() {
        return codeISBN;
    }

    public void setCodeISBN(String codeISBN) {
        this.codeISBN = codeISBN;
    }
}
